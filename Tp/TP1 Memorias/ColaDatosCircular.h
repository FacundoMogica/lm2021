#include <stdint.h>
#define TAM_MAX 2
class stack{
public:
    stack()
    {
        indiceLeer=0;
        indiceEscribir=0;
    }
    uint8_t leer_Dato()
    {
        if (indiceLeer==-1)
        {
            indiceLeer=TAM_MAX;
        }
        if(indiceLeer==indiceEscribir){
            return -1;
        }
        else if (indiceLeer==0)
        {
            indiceLeer=TAM_MAX;
            return mem[0];
        }
        else return mem[indiceLeer--];
                
    }
    void escribir_dato(uint8_t dato)
    {
        if(indiceEscribir==TAM_MAX)
        {
            indiceEscribir=0;
             mem[TAM_MAX]=dato;
        }
        else if ((indiceEscribir+1)==indiceLeer)
        {
           return;
        }
        
        else mem[indiceEscribir++]=dato;
    }
private:
    uint8_t indiceLeer,indiceEscribir; 
    uint8_t mem[TAM_MAX];
};