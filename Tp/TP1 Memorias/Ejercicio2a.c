#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
int32_t varGlobal; 
int32_t Suma(int32_t a, int32_t b)
    {
        int32_t suma= a+b;
        printf("El valor de direccion de a es %d y el de b es %d \n", &a, &b);
        return suma;
    }
int main(void)
{
    
    int32_t var=0;
    int *etext;
    int *edata, *edataGlobal;
    int *edata1=NULL, *edataGlobal1=NULL;
    int (*etext1)(int32_t, int32_t)=NULL;
    etext1=&Suma;
    edata1=&var;
    edataGlobal1=&varGlobal;
    printf("El valor del puntero etext es %d, el de etext1 es %d \nEl valor de edata es %d, el de edata1 es %d y el de edata Global %d, con edata global1 %d\n", etext,etext1,edata,edata1,edataGlobal,edataGlobal1);
    int *end1=NULL;
    int *end;
    varGlobal=0;
    end1=&varGlobal;
     printf("El valor del puntero end es %d, el de end1 es %d\n", end,end1);
     int32_t v1=6,v2=7,v3=Suma(v1,v2);
    printf("El valor de direccion de v1 es %d y el de v2 es %d \n", &v1, &v2);
    system("pause");

}
