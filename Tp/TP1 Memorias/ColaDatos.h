#include <stdint.h>
#define TAM_MAX 500
class stack{
public:
    stack(){indice=0;}
    uint8_t leer_Dato()
    {
        if(indice==0){
            return -1;
        }
        else return mem[indice--];
    }
    void escribir_dato(uint8_t dato)
    {
        if(indice=TAM_MAX)
        {
            return;
        }
        else mem[indice++]=dato;
    }
private:
    uint8_t indice; 
    uint8_t mem[TAM_MAX];
};