import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

Fs=500
Ts=1/Fs
S1=pd.read_csv('.\Tp\TP6 Phyton\presion.csv')

L=len(S1)
k=np.arange(0,L)
t=k*Ts

S2=abs(np.fft.fft(S1.values.flatten()))
rf=Fs/(L-1)
f=k*rf



plt.subplot(211)
plt.plot(t,S1)
plt.grid()
plt.xlabel("t[s]")
plt.ylabel("P[mV]")

plt.subplot(212)
plt.plot(f,S2)
plt.xlabel("f[hz]")
plt.ylabel("H(z)")

plt.show()
