import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

Vmax_sensor=1
Vmin_sensor=-1

Vmax_adc=3.3
Vmin_adc=0

G=(Vmax_adc-Vmin_adc)/(Vmax_sensor-Vmin_sensor)
offset=(Vmax_adc-Vmin_adc)/2

S1=pd.read_csv('.\Tp\TP6 Phyton\presion.csv')
S2=S1*G+offset

S2_max=Vmax_adc
S2_min=Vmin_adc

Fs=500
Ts=1/Fs
L=len(S1)
k=np.arange(0,L)
t=k*Ts

#D1 en 8 bits
N1=8
D1_min=0
D1_max=2**N1-1

#S2---->D1
D1=round(S2*((D1_max-D1_min)/(S2_max-S2_min)))

#D2 en 12 bits
N2=12
D2_min=0
D2_max=2**N2-1

#S2---->D2
D2=round(S2*((D2_max-D2_min)/(S2_max-S2_min)))

#D3 en 24 bits
N3=24
D3_min=0
D3_max=2**N3-1

#S2---->D3
D3=round(S2*((D3_max-D3_min)/(S2_max-S2_min)))
plt.figure(1)
plt.subplot(411)
plt.plot(t,S2)
plt.grid()
plt.xlabel("t[s]")
plt.ylabel("P[mV]")
plt.title("Señal Original")

plt.subplot(412)
plt.plot(t,D1)
plt.grid()
plt.xlabel("t[s]")
plt.ylabel("P[mV]")
plt.title("ADC de 8 bits")

plt.subplot(413)
plt.plot(t,D2)
plt.grid()
plt.xlabel("t[s]")
plt.ylabel("P[mV]")
plt.title("ADC de 12 bits")

plt.subplot(414)
plt.plot(t,D3)
plt.grid()
plt.xlabel("t[s]")
plt.ylabel("P[mV]")
plt.title("ADC de 24 bits")
plt.show()

SS2=abs(np.fft.fft(S2.values.flatten()))
DD1=abs(np.fft.fft(D1.values.flatten()))
DD2=abs(np.fft.fft(D2.values.flatten()))
DD3=abs(np.fft.fft(D3.values.flatten()))
rf=Fs/(L-1)
f=k*rf

plt.figure(2)
plt.subplot(411)
plt.plot(f,SS2)
plt.grid()
plt.xlabel("f[hz]")
plt.ylabel("S2(z)")
plt.title("Señal Original")

plt.subplot(412)
plt.plot(f,DD1)
plt.grid()
plt.xlabel("f[hz]")
plt.ylabel("D1(z)")
plt.title("ADC de 8 bits")

plt.subplot(413)
plt.plot(f,DD2)
plt.grid()
plt.xlabel("f[hz]")
plt.ylabel("D2(z)")
plt.title("ADC de 12 bits")

plt.subplot(414)
plt.plot(f,DD3)
plt.grid()
plt.xlabel("f[hz]")
plt.ylabel("D3(z)")
plt.title("ADC de 24 bits")
plt.show()

S3 = S2 * (D1_max-D1_min)/(S2_max-S2_min)
plt.figure (3)
plt.subplot(311)
plt.stem(t,(S3-D1))
plt.grid()
plt.xlabel("t[s]")
plt.ylabel("Error") 
plt.title("Error 8 bits")

S3 = S2 * (D2_max-D2_min)/(S2_max-S2_min)
plt.subplot(312)
plt.stem(t,(S3-D2))
plt.grid()
plt.xlabel("t[s]")
plt.ylabel("Error") 
plt.title("Error 12 bits")

S3 = S2 * (D3_max-D3_min)/(S2_max-S2_min)
plt.subplot(313)
plt.stem(t,(S3-D3))
plt.grid()
plt.xlabel("t[s]")
plt.ylabel("Error") 
plt.title("Error 24 bits")
plt.show()

SS3 = SS2 * float(max(DD1)/(max(SS2)))
plt.figure(4)
plt.subplot(311)
plt.stem(f,(SS3-DD1))
plt.grid()
plt.xlabel("f[hz]")
plt.ylabel("Error")
plt.title("Error 8 bits")

SS3 = SS2 * float(max(DD2)/(max(SS2)))
plt.subplot(312)
plt.stem(f,(SS3-DD2))
plt.grid()
plt.xlabel("f[hz]")
plt.ylabel("Error")
plt.title("Error 12 bits")

SS3 = SS2 * float(max(DD3)/(max(SS2)))
plt.subplot(313)
plt.stem(f,(SS3-DD3))
plt.grid()
plt.xlabel("f[hz]")
plt.ylabel("Error")
plt.title("Error 24 bits")
plt.show()