import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

Vmax_sensor=1
Vmin_sensor=-1

Vmax_adc=3.3
Vmin_adc=0

G=(Vmax_adc-Vmin_adc)/(Vmax_sensor-Vmin_sensor)
offset=(Vmax_adc-Vmin_adc)/2

S1=pd.read_csv('.\Tp\TP6 Phyton\presion.csv')
S2=S1*G+offset

S2_max=Vmax_adc
S2_min=Vmin_adc

Fs=500
Ts=1/Fs
L=len(S1)
k=np.arange(0,L)
t=k*Ts

plt.subplot(211)
plt.plot(t,S1)
plt.grid()
plt.xlabel("t[s]")
plt.ylabel("P[mV]")
plt.title("Señal Original")

plt.subplot(212)
plt.plot(t,S2)
plt.grid()
plt.xlabel("t[s]")
plt.ylabel("P[mV]")
plt.title("Señal nueva")
plt.show()
